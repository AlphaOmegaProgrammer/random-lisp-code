(let
	(
		(counter 5)
		(limit 3)
	)
	(print 2)
	(print 3)
	(loop 
		(let
			((i 3))
			(loop 
				(if (zerop (rem counter i))
					(return)
					(when (>= i limit)
						(print counter)
						(return)
					)
				)
				(incf i 2)
			)
			(incf counter 2)
			(setq limit (sqrt counter))
			(when (> counter 100)
				(return)
			)
		)
	)
)
