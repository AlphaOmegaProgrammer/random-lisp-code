This was me attempting to learn Common Lisp. Lisp was too much for me back then, but I managed to make a prime number generator at least. I highly recommend writing at least one program in lisp, the tooling is absolutely fascinating.

tbh I'll probably try learning haskell before considering returning to lisp, but I might come back to this one day.
